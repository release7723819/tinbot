package ac

import "sync"

var rbac *RBAC = nil
var mut sync.Mutex = sync.Mutex{}


func Get() *RBAC {
    if rbac == nil {
        panic("RBAC has not yet been set")
    }
    return rbac
}

func Set(newRbac *RBAC) {
    mut.Lock()
    defer mut.Unlock()
    if rbac != nil {
        panic("RBAC has already been set")
    }
    rbac = newRbac
}


