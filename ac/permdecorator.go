package ac

type PermDecorator struct {
    userID UserID
    password string
    rbac *RBAC
    permErrHandler func(*PermissionsError)
    otherErrHandler func(error)
}

func NewPermDecorator(
    owner UserID,
    password string,
    rbac *RBAC,
    permErrHandler func(*PermissionsError),
    otherErrHandler func(error)) *PermDecorator {
    return &PermDecorator{
        userID: owner,
        password: password,
        rbac: rbac,
        permErrHandler: permErrHandler,
        otherErrHandler: otherErrHandler,
    }
}

func (self *PermDecorator) Do2(f func() error, permErrHandler func(*PermissionsError), otherErrHandler func(error)) {
    err := f()
    if err != nil {
        switch err := err.(type) {
        case *PermissionsError: permErrHandler(err)
        default: otherErrHandler(err)
        }
    }
}

func (self *PermDecorator) Do(f func() error) {
    self.Do2(f, self.permErrHandler, self.otherErrHandler)
}

func (self *PermDecorator) DoWithPerm2(user UserID, perm PermID, f func() error, permErrHandler func (*PermissionsError), otherErrHandler func(error)) {
    auth := Auth{self.userID, self.password}
    err := DoWhileAuthVoid(self.rbac, auth, []PermID{perm}, f)
    if err != nil {
        switch err := err.(type) {
        case *PermissionsError: permErrHandler(err)
        default: otherErrHandler(err)
        }
    }
}

func (self *PermDecorator) DoWithPerm(user UserID, perm PermID, f func() error) {
    self.DoWithPerm2(user, perm, f, self.permErrHandler, self.otherErrHandler)
}

func (self *PermDecorator) DoWithPerms2(user UserID, perms []PermID, f func() error, permErrHandler func(*PermissionsError), otherErrHandler func(error)) {
    auth := Auth{self.userID, self.password}
    err := DoWhileAuthVoid(self.rbac, auth, perms, f)
    if err != nil {
        switch err := err.(type) {
        case *PermissionsError: permErrHandler(err)
        default: otherErrHandler(err)
        }
    }
}

func (self *PermDecorator) DoWithPerms(user UserID, perms []PermID, f func() error) {
    self.DoWithPerms2(user, perms, f, self.permErrHandler, self.otherErrHandler)
}

