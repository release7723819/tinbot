package util

type ErrHandlerRes struct {
    // Indicates if the error was handled or not
    Handled bool
    // The following fields are only inspected when Handled is true
    // Indicates if more handlers should be tried even if this one triggered
    FallThrough bool
    // Return the error to the user if and only if this is false
    // If multiple handlers are executed, returns the error to the user
    // if and only if none of the handlers consumed the error
    Consume bool
}

type Handler = func(error) ErrHandlerRes


type ErrorWrapper struct {
    handlers []Handler
}

func NewErrorWrapper() *ErrorWrapper {
    return &ErrorWrapper {
        handlers: make([]Handler, 0, 5),
    }
}

func (self *ErrorWrapper) Handle(err error) error {
    if err == nil {
        return nil
    }
    for _, h := range self.handlers {
        hRes := h(err)
        if !hRes.Handled { continue }
        if hRes.Consume { return nil }
        if !hRes.FallThrough { break }
    }
    return err
}

func (self *ErrorWrapper) HandleOpt(opt *ErrorOpt) error {
    if !opt.HasErr() {
        return nil
    }
    return self.Handle(opt.Err())
}

func (self *ErrorWrapper) HandleOptTotal(opt *ErrorOpt) {
    if !opt.HasErr() {
        return
    }
    self.HandleTotal(opt.Err())
}

func (self *ErrorWrapper) HandleTotal(err error) {
    err = self.Handle(err)
    if err != nil {
        panic("Encountered error when it should have been handled")
    }
}

func (self *ErrorWrapper) WrapVoid(f func() error) error {
    err := f()
    return self.Handle(err)
}

func (self *ErrorWrapper) WrapVoidTotal(f func() error) {
    err := f()
    self.HandleTotal(err)
}

func Wrap[RetType any](self *ErrorWrapper, f func() (RetType, error)) (RetType, error) {
    ret, err := f()
    err = self.Handle(err)
    return ret, err
}

func WrapTotal[RetType any](self *ErrorWrapper, f func() (RetType, error)) RetType {
    ret, err := f()
    self.HandleTotal(err)
    return ret
}

func (self *ErrorWrapper) AddHandler(h Handler) {
    self.handlers = append(self.handlers, h)
}

func AddHandler[ErrType error](self *ErrorWrapper, h func(ErrType) ErrHandlerRes) {
    errHandler := func(e error) ErrHandlerRes {
        switch e.(type) {
        case ErrType: return h(e.(ErrType))
        default: return ErrHandlerRes{false, false, false}
        }
    }
    self.AddHandler(errHandler)
}



