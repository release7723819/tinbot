package util

import (
    "reflect"
)


type void struct {}

type ErrorOpt struct {
    hasVal bool
    val any
    valType reflect.Type
    err error
}

func typeMatch(argType reflect.Type, paramType reflect.Type) bool {
    // We don't do intelligent type matching because Go's type system
    // does weird shit sometimes.
    // For example returning a specific type instead of an interface
    // would result in a panic if we did a nil-check later (which
    // is well defined for interface values)
    return argType == paramType
}

func typeAssertF(fType reflect.Type, expectedArgTypes []reflect.Type, numNormalRetVals int) {
    if fType.Kind() != reflect.Func {
        panic("Provided function value is not a function")
    }
    if fType.NumIn() != len(expectedArgTypes) {
        panic("Provided function value does not take exactly bla arguments")
    }
    for ind, argType := range expectedArgTypes {
        if fType.In(ind) != argType {
            panic("Provided function value has mismatched argument type")
        }
    }
    if fType.NumOut() < numNormalRetVals {
        panic("Provided function value does not return enough values")
    }
    if fType.NumOut() == numNormalRetVals + 1 {
        errRetType := fType.Out(numNormalRetVals)
        if !errRetType.Implements(reflect.TypeFor[error]()) {
            panic("Provided function value does not return an error as final return value")
        }
    } else if fType.NumOut() > numNormalRetVals + 1 {
        panic("Provided function value returns too many values")
    }
}

func ErrorOptFromVal[ValType any](val ValType) ErrorOpt {
    return ErrorOpt{
        hasVal: true,
        val: val,
        valType: reflect.TypeFor[ValType](),
    }
}

func errorOptFromVal(val any, valType reflect.Type) ErrorOpt {
    return ErrorOpt{
        hasVal: true,
        val: val,
        valType: valType,
    }
}

func errorOptFromErr(err error, valType reflect.Type) ErrorOpt {
    return ErrorOpt{
        hasVal: false,
        val: reflect.ValueOf(nil),
        valType: valType,
        err: err,
    }
}

func ErrorOptFromF[ValType any](f func() (ValType, error)) ErrorOpt {
    val, err := f()
    if err != nil {
        return errorOptFromErr(err, reflect.TypeFor[ValType]())
    }
    return ErrorOptFromVal(val)
}

func ErrorOptFromFVoid(f func() error) ErrorOpt {
    err := f()
    if err != nil {
        return errorOptFromErr(err, reflect.TypeFor[void]())
    }
    return ErrorOptFromVal(void{})
}

func (self ErrorOpt) AndThen(f any) ErrorOpt {
    if self.valType == reflect.TypeFor[void]() {
        panic("Self holds a void type")
    }
    fType := reflect.TypeOf(f)
    typeAssertF(fType, []reflect.Type{self.valType}, 1)
    valRetType := fType.Out(0)
    if !self.hasVal {
        return errorOptFromErr(self.err, valRetType)
    }
    fVal := reflect.ValueOf(f)
    retVals := fVal.Call([]reflect.Value{reflect.ValueOf(self.val)})
    valRet := retVals[0]
    var valErr reflect.Value
    if len(retVals) == 1 {
        valErr = reflect.ValueOf(nil)
    } else if len(retVals) == 2 {
        valErr = retVals[1]
    } else {
        panic("Provided function to AndThen does not comply to it's own spec. This is not good.")
    }
    if !valErr.IsNil() {
        return errorOptFromErr(valErr.Interface().(error), valRetType)
    }
    return errorOptFromVal(valRet.Interface(), valRetType)
}

func (self ErrorOpt) AndThenVoid(f any) ErrorOpt {
    fType := reflect.TypeOf(f)
    typeAssertF(fType, []reflect.Type{}, 0)
    if !self.hasVal {
        return errorOptFromErr(self.err, reflect.TypeFor[void]())
    }
    fVal := reflect.ValueOf(f)
    retVals := fVal.Call([]reflect.Value{})
    var valErr reflect.Value
    if len(retVals) == 0 {
        valErr = reflect.ValueOf(nil)
    } else if len(retVals) == 1 {
        valErr = retVals[0]
    } else {
        panic("Provided function to Finally does not comply to it's own spec. This is not good.")
    }
    if !valErr.IsNil() {
        return errorOptFromErr(valErr.Interface().(error), reflect.TypeFor[void]())
    }
    return ErrorOptFromVal(void{})
}

func (self ErrorOpt) HasValue() bool {
    return self.hasVal
}

func (self ErrorOpt) IsVoid() bool {
    return self.valType == reflect.TypeFor[void]()
}

func (self ErrorOpt) HasErr() bool {
    return !self.hasVal
}

func (self ErrorOpt) Err() error {
    if self.hasVal { panic("Self holds no error, it holds a value") }
    return self.err
}

func Get[FinalType any](self ErrorOpt) FinalType {
    if !self.hasVal { panic("Self holds no value, it holds an error") }
    if self.valType == reflect.TypeFor[void]() {
        panic("Self holds a void value, which cannot be retrieved")
    }
    if self.valType != reflect.TypeFor[FinalType]() {
        panic("Provided final type does not match self")
    }
    return self.val.(FinalType)
}

func t1(int) (float64, error) {
    return 0.0, nil
}

func t2(float64) (string, error) {
    return "beerkaas", nil
}

func test() {
    start := ErrorOptFromVal(12)
    start.AndThen(t1).AndThen(t2)
}
