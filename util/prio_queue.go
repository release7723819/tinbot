package util

import "errors"

type PrioQueue[ItemType any] struct {
    qs []IQueue[ItemType]
}

func NewPrioQueue[ItemType any](queues []IQueue[ItemType]) *PrioQueue[ItemType] {
    if len(queues) <= 0 {
        panic("Number of queues must be larger than 0")
    }
    return &PrioQueue[ItemType]{
        qs: queues,
    }
}

func (self *PrioQueue[ItemType]) Len() int {
    total_len := 0
    for _, q := range(self.qs) {
        total_len += q.Len()
    }
    return total_len
}

func (self *PrioQueue[ItemType]) Push(item ItemType, prio int) {
    if prio < 0 || prio >= len(self.qs) {
        panic("Invalid priority provided")
    }
    self.qs[prio].Push(item)
    return
}

func (self *PrioQueue[ItemType]) Pop() (item ItemType, err error) {
    for _, q := range self.qs {
        new_item, e := q.Pop()
        if e == nil {
            item = new_item
            return
        }
    }
    err = errors.New("Cannot pop from an empty queue")
    return
}

