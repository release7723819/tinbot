package util

import (
    "sync"
    "time"
    "errors"
    "slices"
    "math/rand"
)

type WeightedRQueue[ItemType any] struct {
    items []ItemType
    timestamps []time.Time
    mut sync.Mutex
}

func NewWeightedRQueue[ItemType any]() *WeightedRQueue[ItemType] {
    return &WeightedRQueue[ItemType]{
        items: make([]ItemType, 0, 100),
        timestamps: make([]time.Time, 0, 100),
        mut: sync.Mutex{},
    }
}

func (self *WeightedRQueue[ItemType]) Len() int {
    return len(self.items)
}

func (self *WeightedRQueue[ItemType]) Push(item ItemType) {
    now := time.Now()
    // This will fuck with multiple entries being created during the same sec
    self.mut.Lock()
    defer self.mut.Unlock()
    self.items = append(self.items, item)
    self.timestamps = append(self.timestamps, now)
}

func (self *WeightedRQueue[ItemType]) Pop() (item ItemType, err error) {
    self.mut.Lock()
    defer self.mut.Unlock()
    qlen := self.Len()
    if (qlen <= 0) {
        err = errors.New("Cannot pop from empty queue")
        return
    }
    now := time.Now()
    var total_weight float64
    total_weight = 0
    // Exchange this later for reservoir sampling
    for _, t := range self.timestamps {
        total_weight += now.Sub(t).Seconds()
    }
    rng := rand.Float64() * total_weight
    for ind, t := range self.timestamps {
        curr_diff := now.Sub(t).Seconds()
        if rng < curr_diff {
            item = self.items[ind]
            self.items = slices.Delete(self.items, ind, ind+1)
            self.timestamps = slices.Delete(self.timestamps, ind, ind+1)
            return
        }
        // Technically this is a bad idea
        rng -= curr_diff
    }
    panic("How did we get here?")
}

