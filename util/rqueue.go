package util

import (
    "errors"
    "sync"
    "math/rand"
)

type RQueue[ItemType any] struct {
    items map[int] ItemType
    mut sync.Mutex
}

func NewRQueue[ItemType any]() *RQueue[ItemType] {
    return &RQueue[ItemType]{
        items: make(map[int]ItemType),
        mut: sync.Mutex{},
    }
}

func (self *RQueue[ItemType]) Len() int {
    return len(self.items)
}

func (self *RQueue[ItemType]) Push(item ItemType) {
    self.mut.Lock()
    defer self.mut.Unlock()
    self.items[self.Len()] = item
}

func (self *RQueue[ItemType]) Pop() (item ItemType, err error) {
    self.mut.Lock()
    defer self.mut.Unlock()
    qlen := self.Len()
    if (qlen <= 0) {
        err = errors.New("No items in the queue!")
        return
    }
    ind := rand.Intn(qlen)
    item = self.items[ind]
    self.items[ind] = self.items[qlen]
    delete(self.items, qlen)
    return
}

