package util

type IQueue[ItemType any] interface {
    Len() int
    Push(ItemType)
    Pop() (ItemType, error)
}

type IPrioQueue[ItemType any] interface {
    Len() int
    Push(ItemType, int)
    Pop() (ItemType, error)
}

