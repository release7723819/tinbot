package util

import (
    "errors"
    "math"
    "math/rand"
    "sync"
)

type PrioRQueue[ItemType any] struct {
    qs []IQueue[ItemType]
    weights []float64
    total_weight float64
    mut sync.Mutex
}


func NewPrioRQueue[ItemType any](queues []IQueue[ItemType], weights []float64) *PrioRQueue[ItemType] {
    if len(queues) <= 0 {
        panic("Amount of queues must be greater than 0")
    }
    if len(queues) != len(weights) {
        panic("Amount of queues does not match the weights provided")
    }
    total_weight := 0.0
    for _, w := range weights {
        if math.IsNaN(w) || math.IsInf(w, 0) {
            panic("Provided weights are not all finite")
        }
        if w <= 0 {
            panic("Provided weights are not all greater than 0")
        }
        total_weight += w
    }
    return &PrioRQueue[ItemType]{
        qs: queues,
        weights: weights,
        total_weight: total_weight,
        mut: sync.Mutex{},
    }
}

func (self *PrioRQueue[ItemType]) Len() int {
    total_len := 0
    for _, q := range self.qs {
        total_len += q.Len()
    }
    return total_len
}

func (self *PrioRQueue[ItemType]) Push(item ItemType, prio int) {
    if prio < 0 || prio >= len(self.qs) {
        panic("Invlalid priority provided")
    }
    self.qs[prio].Push(item)
}

func (self *PrioRQueue[ItemType]) Pop() (item ItemType, err error) {
    // We have to 'intelligently' pick a queue, picking an empty one would be kina dumb
    // Either we try and try again, but that can lead to inf loop
    // So we should probably add a mutex, check for non-empties and compute a total w
    // Then we do an rng and find the correct q, pop from that one and return
    // We can add convenience later for popping N entries
    self.mut.Lock()
    defer self.mut.Unlock()
    filled_weights := make([]float64, 0, len(self.qs))
    q_idxs := make([]int, 0, len(self.qs))
    total_weight := 0.0
    for ind, q := range self.qs {
        if q.Len() > 0 {
            filled_weights = append(filled_weights, self.weights[ind])
            q_idxs = append(q_idxs, ind)
            total_weight += self.weights[ind]
        }
    }
    if len(filled_weights) == 0 {
        err = errors.New("Cannot pop from an empty queue")
        return
    }
    rng := rand.Float64() // [0, 1)
    rng *= total_weight   // [0, total_weight)
    for ind, w := range filled_weights {
        if rng < w { // [0, w) is the interval we want
            new_item, e := self.qs[q_idxs[ind]].Pop()
            if e != nil {
                panic("Queue should not have been empty")
            }
            item = new_item
            return
        }
        rng -= w
    }
    panic("Control flow should never get here")
}
