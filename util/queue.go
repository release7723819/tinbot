package util


import (
    "errors"
    "sync"
)


type Queue[ItemType any] struct {
    items []ItemType
    mut sync.Mutex
}


func MakeQueue[ItemType any](initial_capacity int) Queue [ItemType] {
    return Queue[ItemType] {
        items: make([]ItemType, 0, initial_capacity),
        mut: sync.Mutex{},
    }
}

func (q *Queue[ItemType]) Pop() (val ItemType, err error) {
    q.mut.Lock()
    defer q.mut.Unlock()
    defer func() {
        if r := recover(); r != nil {
            err = errors.New("No items in the queue!")
        }
    }()
    item := q.items[0]
    q.items = q.items[1:]
    return item, nil
}

func (q *Queue[ItemType]) Push(new_item ItemType) {
    q.mut.Lock()
    defer q.mut.Unlock()
    q.items = append(q.items, new_item)
}

func Len[ItemType any](q *Queue[ItemType]) int {
    return len(q.items)
}

