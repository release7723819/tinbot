
package twitch

import (
    "fmt"
    "sync"
    "net/http"
    "github.com/gorilla/websocket"
    "example/hello/irc"
)

type TwitchUserInfo struct {
    Name string
    DisplayName string
    IsMod bool
    IsVip bool
    IsSubbed bool
    HasTurbo bool
    Badges []string
    Color string
}

type TwitchMessage struct {
    Sender TwitchUserInfo
    Channel string
    Message string
    IsFirstMessage bool
}

type TwitchChat struct {
    accessToken string
    conn *websocket.Conn
    connLock sync.Mutex
    userName string
    channelName string
    ircBroker *irc.Broker
    msgHandler func (TwitchMessage)
}

func NewTwitchChat(user string, token string, channel string) *TwitchChat {
    // For now we don't do input sanitation, which is probably a bad idea
    parser := irc.NewParser()
    broker := irc.NewBroker(parser)
    return &TwitchChat{
        accessToken: token,
        connLock: sync.Mutex{},
        userName: user,
        channelName: channel,
        ircBroker: broker,
        msgHandler: nil,
    }
}

func (self *TwitchChat) SendMessage(msg string) {
    privMsg := irc.CreatePrivMessage(self.channelName, msg)
    self.sendMsg(privMsg)
}

func (self *TwitchChat) SetMsgHandler(handler func(TwitchMessage)) {
    self.msgHandler = handler
}

func (self *TwitchChat) run() {
    for {
        _, buf, err := self.conn.ReadMessage()
        if err != nil { panic(err) }
        msgStr := string(buf)
        fmt.Println("We have received the raw message:", msgStr)
        err = self.ircBroker.DispatchFromString(msgStr)
        if err != nil {
            panic(err)
        }
    }

}

func (self *TwitchChat) sendMsg(msg *irc.Message) {
    self.connLock.Lock()
    defer self.connLock.Unlock()
    // This can fail. Maybe tackle that someday
    self.conn.WriteMessage(websocket.TextMessage, []byte(msg.Encode()))
}

func (self *TwitchChat) Connect() error {
    self.ircBroker.AddCallback(irc.MsgPriv, self.privMsgHandler)
    self.ircBroker.AddCallback(irc.MsgPing, self.pingMsgHandler)
    dialer := websocket.Dialer{}
    hdr := http.Header{}
    conn, resp, err := dialer.Dial("wss://irc-ws.chat.twitch.tv:443", hdr)
    if err != nil { return err }
    resp.Body.Close()
    self.conn = conn
    // We have a connection now
    authMsg := irc.CreatePassMessage("oauth:" + self.accessToken)
    self.sendMsg(authMsg)
    nickMsg := irc.CreateNickMessage(self.userName)
    self.sendMsg(nickMsg)
    // Add a check for correct reply here
    reqMsg := irc.CreateReqMessage([]string{"twitch.tv/commands", "twitch.tv/tags"})
    self.sendMsg(reqMsg)
    joinMsg := irc.CreateJoinMessage(self.channelName)
    self.sendMsg(joinMsg)
    go self.run()
    return nil
}

func boolFromTagStr(str string) bool {
    if str == "1" { return true }
    return false
}

func (self *TwitchChat) pingMsgHandler(msg *irc.Message) {
    pongMsg := irc.CreatePongMessage(msg)
    self.sendMsg(pongMsg)
}

func (self *TwitchChat) privMsgHandler(msg *irc.Message) {
    metadata := msg.GetMetaData()
    dispName := metadata.Source.User
    if metadata.Tags.GetTagCount("display-name") == 1 {
        dispName = metadata.Tags.GetTag("display-name")
    }
    userInfo := TwitchUserInfo {
        Name: metadata.Source.User,
        DisplayName: dispName,
        IsMod : boolFromTagStr(metadata.Tags.GetTag("mod")), // "0" if not mod, "1" if mod
        IsVip: metadata.Tags.HasTag("vip"), // Presence of tag indicates VIP, not it's value
        IsSubbed: boolFromTagStr(metadata.Tags.GetTag("subscriber")), // "0" if not subbed, "1" if subbed
        HasTurbo: boolFromTagStr(metadata.Tags.GetTag("turbo")), // Again the boolean shit
        Badges: metadata.Tags.GetTags("badges"), // We might want to remove the '/bla' at the end
        Color: metadata.Tags.GetTag("color"),
    }
    twitchMsg := TwitchMessage {
        Sender: userInfo,
        Channel: msg.Payload[0].Contents,
        Message: msg.Payload[1].Contents,
        IsFirstMessage: boolFromTagStr(metadata.Tags.GetTag("first-msg")),
    }
    if self.msgHandler != nil {
        self.msgHandler(twitchMsg)
    }
}

