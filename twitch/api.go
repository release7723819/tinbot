package twitch

import "example/hello/ac"

type chatCallback struct {
    pred func(string) bool
    cb func(ac.UserID, string)
}

func (self chatCallback) dispatch(user ac.UserID, message string) {
    if self.pred(message) {
        self.cb(user, message)
    }
}


type TwitchApi struct {
    auth ac.Auth
    chat *TwitchChat
    rbac *ac.RBAC
    chatCallbacks []chatCallback
}

func NewTwitchApi(auth ac.Auth, channel string, token string) *TwitchApi {
    rbac := ac.Get()
    chat := NewTwitchChat(channel, token, channel)
    api := &TwitchApi{
        auth: auth,
        chat: chat,
        rbac: rbac,
        chatCallbacks: make([]chatCallback, 0, 10),
    }
    err := api.chat.Connect()
    api.chat.SetMsgHandler(api.chatMsgHandler)
    if err != nil { panic(err) }
    return api
}

func (self *TwitchApi) SendChatMessage(message string) {
    self.chat.SendMessage(message)
}

func (self *TwitchApi) OnChatMessage(pred func(string) bool, cb func(ac.UserID, string)) {
    newCb := chatCallback{pred, cb}
    self.chatCallbacks = append(self.chatCallbacks, newCb)
}

func (self *TwitchApi) chatMsgHandler(msg TwitchMessage) {
    // Here we need to do le epic RBAC translation
    twUser := &msg.Sender
    id, err := self.rbac.FindUser(twUser.Name)
    if err != nil {
        // User doesn't exist yet, we need to create them
        // Also I don't like the empty password
        id, err = self.rbac.AddUser(twUser.Name, twUser.DisplayName, "", self.auth)
        // Add appropriate roles
        if err != nil { panic(err) }
    }
    for _, ccb := range self.chatCallbacks {
        go ccb.dispatch(id, msg.Message)
    }
}

