package twitch

import (
    "strings"
    "example/hello/io"
    "example/hello/ac"
)

type Source struct {
    api *TwitchApi
    outChan chan io.CommandInput
    inChan chan string
    msgBuf string
}

func NewSource(api *TwitchApi) *Source {
    newSource := &Source{api, make(chan io.CommandInput), make(chan string), ""}
    api.OnChatMessage(
        newSource.isCommandMsg,
        newSource.handleNewMessage,
    )
    go newSource.writeInput()
    return newSource
}

func (self *Source) Chan() chan io.CommandInput {
    return self.outChan
}

func (self *Source) StdOut() io.TextIO {
    return io.CreateChanTextIO(self.inChan)
}

func (self *Source) isCommandMsg(msg string) bool {
    return strings.HasPrefix(msg, "!")
}

func (self *Source) handleNewMessage(user ac.UserID, msg string) {
    self.outChan<-io.CommandInput{
        User: user,
        InpStr: msg[1:], // Remove the leading '!'
    }
}

func (self *Source) writeInput() {
    for s := range self.inChan {
        self.msgBuf += s
        // Only send if we end on a newline
        // This is a heuristic that will likely not always work
        if self.msgBuf[len(self.msgBuf)-1] == '\n' {
            self.api.SendChatMessage(self.msgBuf)
            self.msgBuf = ""
        }
    }
}




