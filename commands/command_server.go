package commands

import (
    "log"
    "errors"
    "example/hello/ac"
    "example/hello/parsing"
    "example/hello/io"
)

func forward(inp chan io.CommandInput, src io.Source, outp chan commandInput) {
    for comm_inp := range inp {
        outp <- commandInput{comm_inp, src}
    }
}

type commandInput struct {
    inp io.CommandInput
    src io.Source
}

type CommandServer struct {
    logger *log.Logger
    sources []io.Source
    command_lut map[string]parsing.ICommandSpec
    in_channel chan commandInput
    exit_channel chan bool
    std_err io.TextIO
}

func NewCommandServer(std_err io.TextIO) *CommandServer {
    // We use buffered channels so we get a non-blocking send
    ret := &CommandServer{
        log.Default(),
        make([]io.Source, 10),
        make(map[string]parsing.ICommandSpec),
        make(chan commandInput, 32),
        make(chan bool, 1), std_err,
    }
    ret.AddCommand(ret.getHelpCommandSpec())
    ret.AddCommand(ret.getExitCommandSpec())
    return ret
}

// Should not be possible when server is runnnig
// -> to simplify forwarding
func (self *CommandServer) AddSource(src io.Source) {
    self.sources = append(self.sources, src)
    // This should ideally be moved to start function
    go forward(src.Chan(), src, self.in_channel)
}

func (self *CommandServer) AddCommand(comm parsing.ICommandSpec) error {
    if _, exists := self.command_lut[comm.Name()]; exists {
        return errors.New("Command with provided name already exists")
    }
    self.command_lut[comm.Name()] = comm
    return nil
}

func (self *CommandServer) run_command(comm parsing.ICommand, user ac.UserID, src io.Source) {
    defer func() {
        if r := recover(); r != nil {
            self.std_err.Println("Command Paniced!!", r)
        }
    }()
    res := comm.Exec(user, src.StdOut(), self.std_err)
    if res != nil {
        self.std_err.Println("Command encountered an error: ", res)
    }
}

type exitCommand struct {
    server *CommandServer
}

func (self exitCommand) Exec(user ac.UserID, StdOut io.TextIO, StdErr io.TextIO) error {
    StdOut.Println("Exiting...")
    self.server.exit_channel<-true
    return nil
    // Somehow, we should exit
}

func (self *CommandServer) getExitCommandSpec() parsing.ICommandSpec {
    return parsing.NewCommandSpec(
        []parsing.ReqArg[EmptyCommandNS]{},
        []parsing.OptArg[EmptyCommandNS]{},
        "exit",
        "Exits the command server",
        NewEmptyCommandNamespace,
        func (ns *EmptyCommandNS) parsing.ICommand {return exitCommand{self}},
    )
}

type helpCommand struct {
    server *CommandServer
}

func (self helpCommand) Exec(user ac.UserID, StdOut io.TextIO, StdErr io.TextIO) error {
    StdOut.Println("All available commands:")
    for _, comm := range self.server.command_lut {
        StdOut.Println(comm.Name(), ": ", comm.Description())
    }
    return nil
}

func (self *CommandServer) getHelpCommandSpec() parsing.ICommandSpec {
    return parsing.NewCommandSpec(
        []parsing.ReqArg[EmptyCommandNS]{},
        []parsing.OptArg[EmptyCommandNS]{},
        "help",
        "Prints this help",
        NewEmptyCommandNamespace,
        func (ns *EmptyCommandNS) parsing.ICommand {return helpCommand{self}},
    )
}

func (self *CommandServer) handle_new_input(inp string, user ac.UserID, src io.Source) {
    tok, err := parsing.NewMyArgTokenizer(inp)
    if (err != nil) {
        self.logger.Output(2, "Received a malformed command")
        return
    }
    if tok.HasAnyToken() && !tok.HasNormalToken() {
        self.logger.Output(2, "Invalid command name")
        return
    }
    if (!tok.HasNormalToken()) {
        return
    }
    // Int arg = calldepth
    self.logger.Output(2, "Handling a new command")
    comm_name := tok.ParseNormalToken()
    var comm_spec parsing.ICommandSpec
    var ok bool
    if comm_spec, ok = self.command_lut[comm_name]; !ok {
        self.logger.Output(2, "Received unknown command!")
        return
    }
    comm, err := comm_spec.Parse(tok)
    if err != nil {
        self.logger.Output(2, "Could not parse command!")
        return
    }
    self.run_command(comm, user, src)
}

func (self *CommandServer) RunCommandServer() {
    for {
        select {
        case new_comm := <-self.in_channel:
            // Handle command in new thread so we can exec commands concurrently
            // Possible future update: use a thread pool to limit max concurrent commands
            // (this also minimises GC for exited goroutines)
            go self.handle_new_input(new_comm.inp.InpStr, new_comm.inp.User, new_comm.src)
        case <-self.exit_channel:
            return
        }
    }
}

