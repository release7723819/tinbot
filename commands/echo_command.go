package commands

import (
    "example/hello/parsing"
    "example/hello/io"
    "example/hello/ac"
)

type EchoNamespace struct {
    args []string
}

type EchoArg struct { }

func (EchoArg) Parse(tok parsing.ArgTokenizer, ns *EchoNamespace) error {
    for tok.HasNormalToken() {
        ns.args = append(ns.args, tok.ParseNormalToken())
    }
    return nil
}

type EchoCommand struct {
    ns *EchoNamespace
}

func NewEchoCommand(ns *EchoNamespace) parsing.ICommand {
    return &EchoCommand{ns}
}

func (self *EchoCommand) Exec(User ac.UserID, StdOut io.TextIO, StdErr io.TextIO) error {
    n_args := len(self.ns.args)
    for _, str := range self.ns.args[:n_args-1] {
        StdOut.Printf("%s ", str)
    }
    StdOut.Println(self.ns.args[n_args-1])
    return nil
}

func GetEchoCommandSpec() parsing.ICommandSpec {
    req_args := make([]parsing.ReqArg[EchoNamespace], 0, 1)
    req_args = append(req_args, EchoArg{})
    opt_args := make([]parsing.OptArg[EchoNamespace], 0, 0)

    return parsing.NewCommandSpec(
        req_args,
        opt_args,
        "echo",
        "Echo's stuff back to the user",
        func() *EchoNamespace { return &EchoNamespace{make([]string, 0, 10)} },
        NewEchoCommand,
    )
}

