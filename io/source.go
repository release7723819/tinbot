package io

import "example/hello/ac"


type CommandInput struct {
    User ac.UserID
    InpStr string
}


type Source interface {
    Chan() chan CommandInput
    StdOut() TextIO
}

