
package io

import "fmt"

type TextIO interface {
    Printf(format string, args... any)
    Println(args... any)
}

type ChanTextIO struct {
    ch chan string
}

func CreateChanTextIO(ch chan string) ChanTextIO {
    return ChanTextIO{ch}
}

func (self ChanTextIO) Printf(format string, args... any) {
    self.ch <- fmt.Sprintf(format, args...)
}

func (self ChanTextIO) Println(args... any) {
    self.ch <- fmt.Sprintln(args...)
}

