package io

import (
    "bufio"
    "os"
    "fmt"
    "example/hello/ac"
)


type StdinSource struct {
    user ac.UserID
    inp_chan chan CommandInput
    out_chan chan string
}


func NewStdinSource(user ac.UserID) *StdinSource {
    return &StdinSource{
        user: user,
        inp_chan: make(chan CommandInput),
        out_chan: make(chan string),
    }
}

func (self *StdinSource) Chan() chan CommandInput {
    return self.inp_chan
}

func (self *StdinSource) StdOut() TextIO {
    return CreateChanTextIO(self.out_chan)
}

func (self *StdinSource) Run() {
    go self.RunOutput()
    scanner := bufio.NewScanner(os.Stdin)
    for scanner.Scan() {
        if scanner.Err() != nil {
            return
        }
        self.inp_chan <- CommandInput{ self.user, scanner.Text() }
    }
}

func (self *StdinSource) RunOutput() {
    for output := range self.out_chan {
        fmt.Printf(output)
    }
}



