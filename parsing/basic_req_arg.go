package parsing

import "errors"


type BasicReqArg[AT any, NS any] struct {
    parse_func func(string) (AT, error)
    export_func func (AT, *NS) error
}

func NewBasicReqArg[AT any, NS any](
    p_func func(string) (AT, error),
    e_func func (AT, *NS) error) *BasicReqArg[AT, NS] {
    return &BasicReqArg[AT, NS]{p_func, e_func}
}

func (self *BasicReqArg[AT, NS]) Parse(tokenizer ArgTokenizer, namespace *NS) error {
    if !tokenizer.HasNormalToken() {
        return errors.New("Not enough arguments")
    }
    val, err := self.parse_func(tokenizer.ParseNormalToken())
    if err != nil {
        return errors.New("Could not parse req arg")
    }
    err = self.export_func(val, namespace)
    if err != nil {
        return errors.New("Could not export req arg")
    }
    return nil
}

