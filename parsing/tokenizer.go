
package parsing

import (
    "strings"
    "unicode"
    "errors"
)

type ArgTokenizer interface {
    HasNormalToken() bool
    ParseNormalToken() string
    HasFlagToken() bool
    ParseFlagToken() string
    HasAnyToken() bool
    ParseAnyToken() string
}

type MyArgTokenizer struct {
    words []string
    next_idx int
}

func findFirstMatching(chars []rune, start int, pred func(rune) bool) (int, error) {
    for start < len(chars) {
        if pred(chars[start]) { return start, nil }
        start += 1
    }
    return 0, errors.New("Could not rune satisfying supplied predicate")
}

func findWordStart(chars []rune, start int) (int, error) {
    return findFirstMatching(chars, start, func(r rune) bool {return !unicode.IsSpace(r)})
}

func findWordEnd(chars []rune, start int) (int, error) {
    return findFirstMatching(chars, start, unicode.IsSpace)
}

func findStringEnd(chars []rune, start int) (int, error) {
    return findFirstMatching(chars, start + 1, func(r rune) bool { return r == chars[start] })
}

func NewMyArgTokenizer(inp string) (ArgTokenizer, error) {
    string_delimiters := map[rune]any{'\'': nil, '"': nil}
    words := make([]string, 0, 5)
    inp = strings.TrimSpace(inp)
    if len(inp) == 0 {
        return &MyArgTokenizer{
            words: words,
            next_idx: 0,
        }, nil
    }
    runes := []rune(inp)
    curr_idx := 0
    for {
        if _, ok := string_delimiters[runes[curr_idx]]; ok {
            // We have a string on our hands
            str_end_idx, err := findStringEnd(runes, curr_idx)
            if (err != nil) {
                return nil, errors.New("Unfinished string found in " + inp)
            }
            words = append(words, string(runes[curr_idx + 1:str_end_idx]))
            curr_idx = str_end_idx + 1
        } else {
            token_end, err := findWordEnd(runes, curr_idx)
            if err != nil { token_end = len(inp) }
            words = append(words, string(runes[curr_idx:token_end]))
            curr_idx = token_end + 1
        }
        new_curr_idx, err := findWordStart(runes, curr_idx)
        if (err != nil) { break } // We got to the end of the input string
        curr_idx = new_curr_idx
    }
    return &MyArgTokenizer{
        words: words,
        next_idx: 0,
    }, nil
}

func (self *MyArgTokenizer) HasAnyToken() bool {
    return self.next_idx < len(self.words)
}

func (self *MyArgTokenizer) ParseAnyToken() string {
    if !self.HasAnyToken() {
        panic("ParseAnyWord called but has no Token")
    }
    ret := self.words[self.next_idx]
    self.next_idx++
    return ret
}

func (self *MyArgTokenizer) HasFlagToken() bool {
    return self.HasAnyToken() && strings.HasPrefix(self.words[self.next_idx], "-")
}

func (self *MyArgTokenizer) ParseFlagToken() string {
    if !self.HasFlagToken() {
        panic("ParseFlagToken called but tokenizer has no such token")
    }
    ret := self.words[self.next_idx]
    self.next_idx++
    return ret
}

func (self *MyArgTokenizer) HasNormalToken() bool {
    return self.HasAnyToken() && !self.HasFlagToken()
}

func (self *MyArgTokenizer) ParseNormalToken() string {
    if !self.HasNormalToken() {
        panic("ParseNormalToken called but tokenizer has no such token")
    }
    ret := self.words[self.next_idx]
    self.next_idx++
    return ret
}

