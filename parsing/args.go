package parsing


type OptArg[NS any] interface {
    Parse(ArgTokenizer, *NS) error
    GetFlags() []string
    MaxCount() int
}

type ReqArg[NS any] interface {
    Parse(ArgTokenizer, *NS) error
}

