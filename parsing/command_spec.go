
package parsing

import (
    "strings"
    "errors"
    "example/hello/io"
    "example/hello/ac"
)

type ICommand interface {
    Exec(User ac.UserID, StdOut io.TextIO, StdErr io.TextIO) error
}

type ICommandSpec interface {
    Parse(ArgTokenizer) (ICommand, error)
    Name() string
    Description() string
}

// We need the ns factory here because otherwise we can't do the 'hide the generics' trick anymore
type CommandSpec[NS any] struct {
    ReqArgs []ReqArg[NS]
    OptArgs []OptArg[NS]
    name string
    description string
    ns_factory func() *NS
    command_factory func(*NS) ICommand
}

func NewCommandSpec[NS any](
    req_args []ReqArg[NS],
    opt_args []OptArg[NS],
    name string,
    description string,
    ns_fac func() *NS,
    comm_fac func(*NS) ICommand) *CommandSpec[NS] {
    return &CommandSpec[NS] {
        ReqArgs: req_args,
        OptArgs: opt_args,
        name: name,
        description: description,
        ns_factory: ns_fac,
        command_factory: comm_fac,
    }
}

func (self *CommandSpec[NS]) Name() string {
    return self.name
}

func (self *CommandSpec[NS]) Description() string {
    return self.description
}

func (self *CommandSpec[NS]) NewEmptyNamespace() *NS {
    return self.ns_factory()
}

func (self *CommandSpec[NS]) FindOptArg(arg_name string) (OptArg[NS], error) {
    opt_arg_found := false
    var res OptArg[NS]
    res = nil
    for _, opt_arg := range self.OptArgs {
        for _, flag := range opt_arg.GetFlags() {
            if strings.HasPrefix(flag, arg_name) {
                if opt_arg_found {
                    return nil, errors.New("Ambigious flag provided")
                }
                opt_arg_found = true
                res = opt_arg
            }
        }
    }
    if res == nil {
        return nil, errors.New("Coult not find any optional argument")
    }
    return res, nil
}

func parse_internal[NS any](tokenizer ArgTokenizer, spec *CommandSpec[NS]) (comm ICommand, err error) {
    defer func() {
        if e := recover(); e != nil {
            err = errors.New("Parsing recovered from panic!")
        }
    }()
    ns := spec.NewEmptyNamespace()
    req_arg_parsed := 0
    for tokenizer.HasAnyToken() {
        if tokenizer.HasFlagToken() {
            flag_token := tokenizer.ParseFlagToken()
            opt_arg, err := spec.FindOptArg(flag_token)
            if err != nil {
                return nil, errors.New("Could not parse opt arg")
            }
            if opt_arg.Parse(tokenizer, ns) != nil {
                return nil, errors.New("Could not feiks opt arg")
            }
        }
        if req_arg_parsed >= len(spec.ReqArgs) {
            return nil, errors.New("Too many arguments provided")
        }
        next_req_arg := spec.ReqArgs[req_arg_parsed]
        if next_req_arg.Parse(tokenizer, ns) != nil {
            return nil, errors.New("Could not feiks req arg")
        }
        req_arg_parsed++
    }
    if req_arg_parsed < len(spec.ReqArgs) {
        return nil, errors.New("Not enough required args provided!")
    }
    return spec.command_factory(ns), nil
}


func (self *CommandSpec[NS]) Parse(tokenizer ArgTokenizer) (ICommand, error) {
    return parse_internal(tokenizer, self)
}

