
package parsing

import "errors"

type BasicOptArg[AT any, NS any] struct {
    short_flag string
    long_flag string
    parse_func func(string) (AT, error)
    export_func func(AT, *NS) error
}

func NewBasicOptArg[AT any, NS any](
    short_flag string,
    long_flag string,
    p_func func(string) (AT, error),
    e_func func(AT, *NS) error) *BasicOptArg[AT, NS] {
    return &BasicOptArg[AT, NS] {
        short_flag,
        long_flag,
        p_func,
        e_func,
    }
}

func (self *BasicOptArg[AT, NS]) GetFlags() []string {
    return []string{self.short_flag, self.long_flag}
}

func (self *BasicOptArg[AT, NS]) MaxCount() int {
    return 1
}

func (self *BasicOptArg[AT, NS]) Parse(tokenizer ArgTokenizer, namespace *NS) error {
    if (!tokenizer.HasNormalToken()) {
        return errors.New("Not enough arguments")
    }
    val, err := self.parse_func(tokenizer.ParseNormalToken())
    if err != nil {
        return errors.New("Could not parse shit")
    }
    err = self.export_func(val, namespace)
    if err != nil {
        return errors.New("Could not export val to ns")
    }
    return nil
}
