package main

import (
    "fmt"
    "os"
    "log"
    "crypto/sha512"
    "example/hello/util"
    "example/hello/io"
    "example/hello/commands"
    "example/hello/twitch"
    "example/hello/twitch_queue"
    "example/hello/ac"
)

func main() {
    // Create RBAC
    // All this should sometime be moved to a generic orchestration mechanism
    args := os.Args
    if len(args) != 3 {
        panic(fmt.Sprintf("Expected 2 arguments, but got %d", len(args)))
    }
    channelName := args[1]
    token := args[2]
    rbac, rootUserID := ac.NewRBAC(sha512.New(), "toor")
    ac.Set(rbac)
    rootAuth := ac.Auth{User: rootUserID, Pw: "toor"}

    appRole, _ := rbac.AddRole("AppRole", rootAuth)
    rbac.AssignPerm(appRole, ac.PermAddUser, rootAuth)
    rbac.AssignPerm(appRole, ac.PermAddPerm, rootAuth)
    rbac.AssignPerm(appRole, ac.PermAssignPerm, rootAuth)
    rbac.AssignPerm(appRole, ac.PermCheckRoles, rootAuth)
    rbac.AssignPerm(appRole, ac.PermCheckPerms, rootAuth)
    twitchUser, _ := rbac.AddUser("Twitch", "Twitch", "pw1", rootAuth)
    rbac.AssignRole(twitchUser, appRole, rootAuth)
    stdinUser, _ := rbac.AddUser("StdIn", "ADisplayName", "pw2", rootAuth)
    rbac.AssignRole(stdinUser, appRole, rootAuth)

    twApi := twitch.NewTwitchApi(rootAuth, channelName, token)
    twSource := twitch.NewSource(twApi)

    // Set RBAC singleton
    var stdin_source *io.StdinSource
    stdin_source = io.NewStdinSource(stdinUser)
    server := commands.NewCommandServer(stdin_source.StdOut())
    var t_q *twitch_queue.TwitchQueue
    // Why is that ugly wrapping necessary?
    t_q = twitch_queue.NewTwitchQueue(
        func() util.IQueue[ac.UserID] {
            return util.NewWeightedRQueue[ac.UserID]()
        },
        ac.Auth{User: twitchUser, Pw: "pw1"},
    )
    t_q.AllowPickUsers(appRole)
    go stdin_source.Run()
    if server.AddCommand(commands.GetEchoCommandSpec()) != nil {
        log.Fatal("Could not add command pls")
    }
    for _, comm := range t_q.GetCommands() {
        server.AddCommand(comm)
    }
    server.AddSource(stdin_source)
    server.AddSource(twSource)
    server.RunCommandServer()
}
