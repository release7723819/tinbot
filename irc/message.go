package irc

import "strings"


type Message struct {
    MetaData *MetaData
    Payload []Field
}

func (self *Message) GetMetaData() *MetaData {
    return self.MetaData
}

func (self *Message) EncodeData() string {
    payloadStr := make([]string, len(self.Payload), len(self.Payload))
    for i, f := range self.Payload {
        payloadStr[i] = f.Encode()
    }
    return strings.Join(payloadStr, " ")
}

func (self *Message) Encode() string {
    return self.MetaData.Encode() + " " + self.EncodeData()
}

func SplitMessages(messages string) []string {
    raw := strings.Split(messages, "\r\n")
    res := make([]string, 0, len(raw))
    for _, str := range raw {
        str = strings.TrimSpace(str)
        if len(str) != 0 {
            res = append(res, str)
        }
    }
    return res
}

