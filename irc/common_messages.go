package irc

import (
    "strings"
    "errors"
)


const (
    MsgPriv = "PRIVMSG"
    MsgNotice = "NOTICE"
    MsgPass = "PASS"
    MsgJoin = "JOIN"
    MsgNick = "NICK"
    MsgPing = "PING"
    MsgPong = "PONG"
    MsgReq = "CAP" // This would need revising if we add more CAP messages later
)


func CreatePrivMessage(channel string, message string) *Message {
    channelField := CreateField(ChannelField, channel)
    testField := CreateField(TextField, message)
    kanker := []Field{channelField, testField}
    return &Message {
        MetaData: &MetaData{Type: MsgPriv},
        Payload: kanker,
    }
}

func ParsePrivMessage(metaData *MetaData, fields []Field) (*Message, error){
    if len(fields) < 2 {
        return nil, errors.New("Invalid format")
    }
    if fields[0].Kind != ChannelField {
        return nil, errors.New("kakapipi")
    }
    if fields[1].Kind != TextField {
        return nil, errors.New("Brie")
    }
    msg := &Message{
        MetaData: metaData,
        Payload: fields,
    }
    return msg, nil
}

func CreateNoticeMessage(notice string) *Message {
    return &Message{
        MetaData: &MetaData{Type: MsgNotice},
        Payload: []Field{CreateField(GenericField, "*"), CreateField(TextField, notice)},
    }
}

// Again, it might be worth to bake in support for 'text' fields (starting with :)
func ParseNoticeMessage(metaData *MetaData, fields []Field) (*Message, error) {
    if len(fields) != 2 {
        return nil, errors.New("Invalid format")
    }
    if fields[0].Kind != GenericField || fields[0].Contents != "*" {
        return nil, errors.New("")
    }
    if fields[1].Kind != TextField {
        return nil, errors.New("")
    }
    return &Message{
        MetaData: metaData,
        Payload: fields,
    }, nil
}

func CreatePingMessage(payload string) *Message {
    return &Message{
        MetaData: &MetaData{Type: MsgNotice},
        Payload: []Field{ CreateField(TextField, payload) },
    }
}

func ParsePingMessage(metaData *MetaData, fields []Field) (*Message, error) {
    if len(fields) != 1 || fields[0].Kind != TextField {
        return nil, errors.New("Beerkaas")
    }
    return &Message{
        MetaData: metaData,
        Payload: fields,
    }, nil
}

func CreatePongMessage(pingMsg *Message) *Message {
    if pingMsg.MetaData.Type != MsgPing {
        panic("Cannot create pong message from non ping message")
    }
    return &Message {
        MetaData: &MetaData{Type: MsgPing},
        Payload: pingMsg.Payload,
    }
}

func CreatePassMessage(password string) *Message {
    return &Message{
        MetaData: &MetaData{Type: MsgPass},
        Payload: []Field{ CreateField(GenericField, password) },
    }
}


func CreateNickMessage(nickname string) *Message {
    return &Message {
        MetaData: &MetaData{Type: MsgNick},
        Payload: []Field{ CreateField(GenericField, nickname) },
    }
}

func CreateJoinMessage(channel string) *Message {
    return &Message {
        MetaData: &MetaData{Type: MsgJoin},
        Payload: []Field{ CreateField(ChannelField, channel) },
    }
}

func CreateReqMessage(caps []string) *Message {
    return &Message{
        MetaData: &MetaData{Type: MsgReq},
        Payload: []Field{ CreateField(GenericField, "REQ"), CreateField(TextField, strings.Join(caps, " ")) },
    }
}

