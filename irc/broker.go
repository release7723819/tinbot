package irc

import "errors"


type Broker struct {
    parser *Parser
    callbacks map[string] func(*Message)
}

func NewBroker(parser *Parser) *Broker {
    return &Broker{
        parser: parser,
        callbacks: make(map[string] func(*Message)),
    }
}

func (self *Broker) AddCallback(msgId string, callback func(*Message)) error {
    _, ok := self.callbacks[msgId]
    if ok {
        return errors.New("There is already a callback registered")
    }
    self.callbacks[msgId] = callback
    return nil
}

func (self *Broker) DispatchMessage(msg *Message) error {
    cb, ok := self.callbacks[msg.GetMetaData().Type]
    if !ok {
        return errors.New("No callback registered for msg type")
    }
    go cb(msg)
    return nil
}

func (self *Broker) DispatchFromString(encodedMessages string) error {
    messageSlice := SplitMessages(encodedMessages)

    for _, encodedMsg := range messageSlice {
        msg, err := self.parser.ParseMessage(encodedMsg)
        if err != nil {
            // Or we could log this and move on
            return err
        }
        self.DispatchMessage(msg)
    }
    return nil
}

