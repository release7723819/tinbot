package irc

import (
    "errors"
    "strings"
)

type FieldKind int

const (
    TagField FieldKind = iota
    SourceField
    CommandField
    ChannelField
    TextField
    GenericField
)

func getFieldPrefix(kind FieldKind) string {
    // I guess we hope Go is smart enough to make this global static
    lut := map[FieldKind] string {
        TagField: "@",
        SourceField: ":",
        CommandField: "",
        ChannelField: "#",
        TextField: ":",
        GenericField: "",
    }
    return lut[kind]
}

type Field struct {
    Kind FieldKind
    Contents string
}

func (self Field) Encode() string {
    return getFieldPrefix(self.Kind) + self.Contents
}

func CreateField(kind FieldKind, content string) Field {
    // Prefixes are added later
    return Field{
        Kind: kind,
        Contents: content,
    }
}

func parseField(kind FieldKind, rawStr string) (f Field, err error) {
    expectedPrefix := getFieldPrefix(kind)
    if !strings.HasPrefix(rawStr, expectedPrefix) {
        err = errors.New("Expected prefix does not match provided string")
        return
    }
    f = Field {
        Kind: kind,
        Contents: rawStr[len(expectedPrefix):],
    }
    err = nil
    return
}
