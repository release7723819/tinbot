package irc

import (
    "errors"
    "strings"
)

type Tags struct {
    tags map[string] []string
}

func (self Tags) AddTag(key string, value string) {
    existingTags, ok := self.tags[key]
    if ok {
        existingTags = append(existingTags, value)
    } else {
        existingTags = []string{value}
    }
    self.tags[key] = existingTags
}

// PROBLEM: tags can have multiple values
func (self Tags) HasTag(tagName string) bool {
    _, ok := self.tags[tagName]
    return ok
}

func (self Tags) GetTagCount(tagName string) int {
    return len(self.tags[tagName])
}

func (self Tags) GetTag(tagName string) string { 
    values := self.tags[tagName]
    if len(values) != 1 { panic("Provided tag doesn't have a single value") }
    return values[0]
}

func (self Tags) GetTags(tagName string) []string {
    return self.tags[tagName]
}

func (self Tags) Encode() string {
    if len(self.tags) == 0 { return "" }
    kvPairs := make([]string, 0, len(self.tags))
    for key, values := range self.tags {
        kvPairs = append(kvPairs, key + "=" + strings.Join(values, ","))
    }
    return getFieldPrefix(TagField) + strings.Join(kvPairs, ";")
}

func parseTags(tagField Field) (tags Tags, err error) {
    if tagField.Kind != TagField {
        err = errors.New("Invalid field")
        return
    }
    tagLut := make(map[string] []string)
    payload := tagField.Contents
    kvPairs := strings.Split(payload, ";")
    for _, pair := range kvPairs {
        key, value, found := strings.Cut(pair, "=")
        if !found {
            err = errors.New("Malformed key value pair")
            return
        }
        if strings.Contains(value, "=") {
            err = errors.New("Value contains '='")
            return
        }
        values := strings.Split(value, ",")
        tagLut[key] = values
    }
    tags = Tags{tagLut}
    return
}

type Source struct {
    Nick string
    User string
    Host string
}

func (self Source) HasNick() bool {
    return len(self.Nick) > 0
}

func (self Source) HasUser() bool {
    return len(self.User) > 0
}

func (self Source) HasHost() bool {
    return len(self.Host) > 0
}

func (self Source) Encode() string {
    res := ""
    if self.HasNick() {
        res += self.Nick + "!"
    }
    if self.HasUser() {
        res += self.User + "@"
    }
    if self.HasHost() {
        res += self.Host
    }
    return getFieldPrefix(SourceField) + res
}

func parseSource(sourceField Field) (src Source, err error) {
    if sourceField.Kind != SourceField {
        err = errors.New("Invalid field kind")
        return
    }
    // fmt: nick!user@host
    nick, remainder, hasNick := strings.Cut(sourceField.Contents, "!")
    if !hasNick {
        remainder = nick
        nick = ""
    }
    user, remainder, hasUser := strings.Cut(remainder, "@")
    if !hasUser {
        remainder = user
        user = ""
    }
    host := remainder
    return Source {
        Nick: nick,
        User: user,
        Host: host,
    }, nil
}

type MetaData struct {
    Tags Tags
    Source Source
    Type string // Maybe not a string?
}

func (self MetaData) Encode() string {
    tagsEncoded := self.Tags.Encode()
    sourceEncoded := self.Source.Encode()

    res := tagsEncoded + " " + sourceEncoded + " " + self.Type
    return strings.TrimSpace(res)
}
