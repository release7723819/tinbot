package irc

import (
    "errors"
    "unicode"
)

type Parser struct {
    msgParsers map[string] func(*MetaData, []Field) (*Message, error)
}

func NewParser() *Parser {
    defaultParsers := make(map[string] func(*MetaData, []Field) (*Message, error))
    defaultParsers[MsgNotice] = ParseNoticeMessage
    defaultParsers[MsgPriv] = ParsePrivMessage
    return &Parser{defaultParsers}
}

func findWord(message []rune, start int) (int, int) {
    // Add in some error handling, now we'll panic
    if start >= len(message) { return len(message), len(message) }
    // This words because Twitch never sends actual unicode
    // If we encounter a non-ASCII token, we'd be in trouble
    for start < len(message) {
        if !unicode.IsSpace(message[start]) { break }
        start++
    }
    if start >= len(message) {
        return len(message), len(message)
    }
    end := start + 1
    for end < len(message) {
        if unicode.IsSpace(message[end]) { break }
        end++
    }
    return start, end
}

func Fields(message string) (fs []Field, err error) {
    // First we look for the Command: the first word with no special prefix
    res := make([]Field, 0, 0)
    runes := []rune(message)
    wordStart, wordEnd := findWord(runes, 0)
    if wordEnd <= wordStart {
        err = errors.New("Cheese")
        return
    }
    nextToken := message[wordStart:wordEnd]
    if nextToken[0:1] == getFieldPrefix(TagField) {
        newTagField, e := parseField(TagField, nextToken)
        // We checked the prefix, err should be nil
        if e != nil { panic(e) }
        res = append(res, newTagField)
        wordStart, wordEnd = findWord(runes, wordEnd)
        if wordEnd <= wordStart {
            err = errors.New("Cheese")
            return
        }
        nextToken = message[wordStart:wordEnd]
    }
    if nextToken[0:1] == getFieldPrefix(SourceField) {
        newSourceField, e := parseField(SourceField, nextToken)
        // We checked the prefix, err should be nil
        if e != nil { panic(e) }
        res = append(res, newSourceField)
        wordStart, wordEnd = findWord(runes, wordEnd)
        if wordEnd <= wordStart {
            err = errors.New("Cheese")
            return
        }
        nextToken = message[wordStart:wordEnd]
    }
    newCommField, err := parseField(CommandField, nextToken)
    if err != nil { panic(err) }
    res = append(res, newCommField)
    wordStart, wordEnd = findWord(runes, wordEnd)
    // We can now only encounter Channel, Text and Generic fields
    for wordEnd > wordStart {
        nextToken = message[wordStart:wordEnd]
        if nextToken[0:1] == getFieldPrefix(ChannelField) {
            newChannelField, err := parseField(ChannelField, nextToken)
            if err != nil { panic(err) }
            res = append(res, newChannelField)
        } else if nextToken[0:1] == getFieldPrefix(TextField) {
            newTextField, err := parseField(TextField, message[wordStart:])
            if err != nil { panic(err) }
            res = append(res, newTextField)
            return res, nil
        } else {
            newGenericField, err := parseField(GenericField, nextToken)
            if err != nil { panic(err) }
            res = append(res, newGenericField)
        }
        wordStart, wordEnd = findWord(runes, wordEnd)
    }
    return res, nil
}

func (self Parser) ParseMessage(message string) (*Message, error) {
    fields, err := Fields(message)
    if err != nil { return nil, err }
    currIdx := 0
    var tags Tags
    if fields[currIdx].Kind == TagField {
        tags, err = parseTags(fields[currIdx])
        if err != nil { return nil, err }
        currIdx++
    }
    var source Source
    if fields[currIdx].Kind == SourceField {
        source, err = parseSource(fields[currIdx])
        if err != nil { return nil, err }
        currIdx++
    }

    if fields[currIdx].Kind != CommandField {
        return nil, errors.New("Gorgonzola")
    }
    command := fields[currIdx].Contents

    metaData := MetaData{
        Tags: tags,
        Source: source,
        Type: command,
    }

    commParser, ok := self.msgParsers[command]
    if ok {
        parsedMsg, err := commParser(&metaData, fields[currIdx+1:])
        if err != nil { return nil, err }
        return parsedMsg, nil
    }
    return &Message{
        MetaData: &metaData,
        Payload: fields[currIdx+1:],
    }, nil
}

