package twitch_queue

import (
    "errors"
    "strconv"
    "fmt"
    "example/hello/util"
    "example/hello/parsing"
    "example/hello/io"
    "example/hello/ac"
)

type PrioType = int

const (
    Subscriber PrioType = iota // iota = 0 and auto increments
    Redeem
    Chatter
)

type TwitchQueue struct {
    q util.IPrioQueue[ac.UserID]
    ourAuth ac.Auth
    joinPayingPerm ac.PermID
    joinFastPerm ac.PermID
    pickUsersPerm ac.PermID
    rbac *ac.RBAC
    wrapper *util.ErrorWrapper
}

func NewTwitchQueue(q_fac func() util.IQueue[ac.UserID], auth ac.Auth) *TwitchQueue {
    qs := []util.IQueue[ac.UserID]{q_fac(), q_fac(), q_fac()}
    weights := []float64{4, 2, 1}
    prio_q := util.NewPrioRQueue(qs, weights)

    wrapper := util.NewErrorWrapper()
    util.AddHandler(
        wrapper,
        func(err *ac.PermissionsError) util.ErrHandlerRes {
            panic(fmt.Sprintf("Insufficient Permissions: %s", err.Error()))
        },
    )

    rbac := ac.Get()
    var payingPerm, fastPerm, pickPerm ac.PermID
    var err error
    opt := util.ErrorOptFromFVoid(
        func() error { payingPerm, err = rbac.AddPerm("Join Paying Users", auth); return err },
    ).AndThenVoid(
        func() error { fastPerm, err = rbac.AddPerm("Join Fastpass Users", auth); return err },
    ).AndThenVoid(
        func() error {pickPerm, err = rbac.AddPerm("Pick Users from Queue", auth); return err },
    )
    wrapper.HandleOptTotal(&opt)

    return &TwitchQueue{prio_q, auth, payingPerm, fastPerm, pickPerm, rbac, wrapper}
}

// Panics if there is a permissions error.
func (self *TwitchQueue) AllowJoinPaid(role ac.RoleID) {
    self.wrapper.WrapVoidTotal(
        func() error {
            return self.rbac.AssignPerm(role, self.joinPayingPerm, self.ourAuth)
        },
    )
}

func (self *TwitchQueue) AllowJoinFast(role ac.RoleID) {
    self.wrapper.WrapVoidTotal(
        func() error {
            return self.rbac.AssignPerm(role, self.joinFastPerm, self.ourAuth)
        },
    )
}

func (self *TwitchQueue) AllowPickUsers(role ac.RoleID) {
    self.wrapper.WrapVoidTotal(
        func() error {
            return self.rbac.AssignPerm(role, self.pickUsersPerm, self.ourAuth)
        },
    )
}

func (self *TwitchQueue) AddName(NewUser ac.UserID, StdOut io.TextIO, StdErr io.TextIO) error {
    var prio PrioType
    if ok, _ := self.rbac.HasPerm(NewUser, self.joinPayingPerm, self.ourAuth); ok {
        prio = Subscriber
    } else if ok, _ := self.rbac.HasPerm(NewUser, self.joinFastPerm, self.ourAuth); ok {
        prio = Redeem
    } else {
        prio = Chatter
    }

    self.q.Push(NewUser, prio)
    StdOut.Printf("Welcome to the queue, @%s\n", self.rbac.GetUserDisplayname(NewUser))
    return nil
}

func (self *TwitchQueue) PullNames(puller ac.UserID, amount int, StdOut io.TextIO, StdErr io.TextIO) error {
    ok, err := self.rbac.HasPerm(puller, self.pickUsersPerm, self.ourAuth)
    self.wrapper.HandleTotal(err)
    if !ok {
        return ac.NewPermissionsError(puller, self.pickUsersPerm, self.rbac)
    }
    max_num := amount
    if max_num < 0 {
        panic("Amount of names provided to PullNames was negative")
    }
    ret := make([]string, 0, max_num)
    for range max_num {
        new_elem, err := self.q.Pop()
        if err != nil {
            break
        }
        ret = append(ret, self.rbac.GetUserDisplayname(new_elem))
    }
    StdOut.Println("Picked the following peeps: ", ret)
    return nil
}

type AddNameNamespace struct {
    name string
    prio PrioType
}

type AddNameCommand struct {
    queue *TwitchQueue
    ns *AddNameNamespace
}

func (self *AddNameCommand) Exec(User ac.UserID, StdOut io.TextIO, StdErr io.TextIO) error {
    return self.queue.AddName(User, StdOut, StdErr)
}

func (self *TwitchQueue) getAddNameCommandSpec() parsing.ICommandSpec {
    return parsing.NewCommandSpec(
        []parsing.ReqArg[AddNameNamespace]{},
        []parsing.OptArg[AddNameNamespace]{},
        "join",
        "Join the queue! YIPPIE",
        func() *AddNameNamespace {return &AddNameNamespace{}},
        func (ns *AddNameNamespace) parsing.ICommand { return &AddNameCommand{self, ns}},
    )
}

type PullNamesNamespace struct {
    amount int
}

type PullNamesCommand struct {
    queue *TwitchQueue
    ns *PullNamesNamespace
}

func (self *PullNamesCommand) Exec(User ac.UserID, StdOut io.TextIO, StdErr io.TextIO) error {
    return self.queue.PullNames(User, self.ns.amount, StdOut, StdErr)
}

func (self *TwitchQueue) getPullNamesCommandSpec() parsing.ICommandSpec {
    return parsing.NewCommandSpec(
        []parsing.ReqArg[PullNamesNamespace]{parsing.NewBasicReqArg(
            func(inp string) (int, error) {
                amount, err := strconv.Atoi(inp)
                if err != nil {return 0, err}
                if amount < 0 {return 0, errors.New("Amount passed to pull names must be positive")}
                return amount, nil;
            },
            func(amount int, ns *PullNamesNamespace) error {ns.amount = amount; return nil;}),
        },
        []parsing.OptArg[PullNamesNamespace]{},
        "pick",
        "Pick a number of people from the queue",
        func() *PullNamesNamespace {return &PullNamesNamespace{}},
        func (ns *PullNamesNamespace) parsing.ICommand { return &PullNamesCommand{self, ns}},
    )
}

func (self *TwitchQueue) GetCommands() []parsing.ICommandSpec {
    return []parsing.ICommandSpec{
        self.getAddNameCommandSpec(),
        self.getPullNamesCommandSpec(),
    }
}

